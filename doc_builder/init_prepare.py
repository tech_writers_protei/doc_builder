# -*- coding: utf-8 -*-
from json import loads

from doc_builder.constants import CustomMethod
from doc_builder.gitlab.api import GitlabAPI, ListProjects, ListProjectBranches, ListProjectUsers
from doc_builder.gitlab.api_parser import gitlab_api_parser
from doc_builder.gitlab.project import GitProject, BranchInfo, UserInfo


def get_projects():
    list_projects: GitlabAPI = ListProjects()
    response_projects = list_projects.http_response_chunked(CustomMethod.GET)
    gitlab_api_parser.response = response_projects

    for item in loads(str(gitlab_api_parser)):
        project_id: int = item["id"]
        project_name: str = item["name"]
        web_url: str = item["web_url"]
        git_project: GitProject = GitProject(project_id, project_name, web_url)
        projects[project_id] = git_project


def get_branches():
    for project_id in projects:
        list_branches: GitlabAPI = ListProjectBranches(project_id)
        response_branches = list_branches.http_response_chunked(CustomMethod.GET)
        gitlab_api_parser.response = response_branches

        for item in loads(str(gitlab_api_parser)):
            branch_name: str = item["name"]
            web_url: str = item["web_url"]
            branch: BranchInfo = BranchInfo(branch_name, web_url)
            projects[project_id] + branch


def get_users():
    for project_id in projects:
        list_users: GitlabAPI = ListProjectUsers(project_id)
        response_users = list_users.http_response_chunked(CustomMethod.GET)
        gitlab_api_parser.response = response_users

        for item in loads(str(gitlab_api_parser)):
            user_id: int = item["id"]
            name: str = item["name"]
            username: str = item["username"]
            user: UserInfo = UserInfo(user_id, name, username)
            projects[project_id] + user


if __name__ == '__main__':
    get_projects()
    get_branches()
    get_users()
    print(projects.values())
