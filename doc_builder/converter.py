# -*- coding: utf-8 -*-
from doc_builder.ascii_doc.include_file import AsciiDocIncludeFile
from doc_builder.constants import FileType
from doc_builder.terminal_command import TerminalCommand


class Converter:
    def __init__(
            self,
            from_type: FileType,
            to_type: FileType,
            include_file: AsciiDocIncludeFile,
            command: TerminalCommand):
        self._from_type: FileType = from_type
        self._to_type: FileType = to_type
        self._include_file: AsciiDocIncludeFile = include_file
        self._command: TerminalCommand = command

    def convert(self):
        self._command.execute_command()

    def __str__(self):
        return (
            f"<{self.__class__.__name__}>"
            f"({str(self._from_type)}, "
            f"{str(self._to_type)}, "
            f"{str(self._include_file)})")

    __repr__ = __str__


class PDFConverter(Converter):
    def __init__(self, from_type: FileType, include_file: AsciiDocIncludeFile):
        to_type: FileType = FileType.PDF
        command: TerminalCommand = TerminalCommand("asciidoctor-pdf")
        super().__init__(from_type, to_type, include_file, command)