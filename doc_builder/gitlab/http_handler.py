# -*- coding: utf-8 -*-
from base64 import b64decode
from http.client import HTTPResponse
from json import loads
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, NamedTuple
from urllib.parse import quote_plus
from urllib.request import Request, urlopen

from loguru import logger

from doc_builder.constants import CHUNK_SIZE, CustomPort, CustomScheme, PROTOCOL, path_temp, validate_path, PathType
from doc_builder.errors import RequiredAttributeMissingError, MissingAttributeError


class CustomHTTPRequest:
    identifier = 0

    def __init__(
            self, *,
            scheme: str | None = None,
            web_hook: str | None = None,
            host: str | None = None,
            port: int | None = None,
            params: tuple[tuple[str, str], ...] | None = None):
        if scheme is None:
            scheme: str = CustomScheme.HTTPS.value
        if port is None:
            port: int = CustomPort.HTTPS.value
        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: int = port
        self._params: tuple[tuple[str, str], ...] = params
        self.index: int = self.__class__.identifier

        self.__class__.identifier += 1

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self._scheme}, web hook = {self._web_hook}, " \
               f"host = {self._host}, post = {self._port}, params = {self._params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._scheme}, {self._web_hook}, {self._host}, {self._port}," \
               f" {self._params})>"

    def __hash__(self):
        return hash(self.url())

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url() == other.url()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url() != other.url()
        else:
            return NotImplemented

    def _get_params(self) -> str:
        if self._params is None:
            return ""
        _params = "&".join([f"{name}={value}" for name, value in self._params])
        return f"?{_params}"

    def _get_web_hook(self) -> str:
        if self._web_hook is None:
            logger.error("Не указан web hook")
            raise RequiredAttributeMissingError
        return self._web_hook.rstrip("/")

    def _get_scheme(self) -> str:
        if self._scheme is None:
            return "https"
        return self._scheme.lower().strip("/")

    def _get_port(self) -> int:
        if self._port is None:
            return CustomPort[PROTOCOL].value
        return self._port

    def _get_host(self) -> str:
        if self._host is None:
            logger.error("Не указан адрес host")
            raise RequiredAttributeMissingError
        return self._host.strip("/")

    def url(self) -> str:
        _url: str = f"{self._get_scheme()}://{self._get_host()}:{self._get_port()}/{self._web_hook}{self._get_params()}"
        return quote_plus(_url, ":/&?=")

    @property
    def host(self) -> str:
        return self._host


class CustomPreparedRequest(NamedTuple):
    url: str
    data: bytes
    headers: dict[str, str]
    host: str
    unverifiable: bool
    method: str
    index: int
    archive: str | Path | None = None

    def __str__(self):
        return f"Соединение #{self.index}:\n{str(self.request().full_url)}"

    def __repr__(self):
        return f"<{self.__class__.__name__}: Connection #{self.index} ({self.url}, {self.data}, " \
               f"{[header for header in self.headers]}, " \
               f"{self.host}, {self.unverifiable}, {self.method})>"

    def __hash__(self):
        return hash((self.host, self.headers, self.data))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.request() == other.request()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.request() != other.request()
        else:
            return NotImplemented

    @classmethod
    def from_custom_request(
            cls,
            custom_http_request: CustomHTTPRequest,
            method: str = None,
            data: bytes = None,
            headers: dict[str, str] = None,
            archive: str | Path | None = None):
        if headers is None:
            headers = dict()

        if data is None:
            data = b''

        if archive is None:
            return cls(
                custom_http_request.url(),
                data,
                headers,
                custom_http_request.host,
                False,
                method,
                custom_http_request.index)

        else:
            return cls(
                custom_http_request.url(),
                data,
                headers,
                custom_http_request.host,
                False,
                method,
                custom_http_request.index,
                archive)

    def request(self) -> Request:
        if self.headers is None:
            self.headers = dict()
        return Request(self.url, self.data, self.headers, self.host, self.unverifiable, self.method)

    def response(self) -> bytes:
        req: HTTPResponse
        with urlopen(self.request()) as req:
            _: bytes = req.read()
        return _

    def http_response(self) -> HTTPResponse:
        return urlopen(self.request())

    def http_response_chunked(self) -> 'CustomHTTPResponseChunked':
        if self.archive is None:
            print("None")
            custom_http_response_chunked: CustomHTTPResponseChunked
            custom_http_response_chunked = CustomHTTPResponseChunked(self.http_response())
        else:
            custom_http_response_chunked: CustomHTTPResponseChunked
            custom_http_response_chunked = CustomHTTPResponseChunked(self.http_response(), archive=self.archive)
        # custom_http_response_chunked.get_response()
        return custom_http_response_chunked


class CustomHTTPResponse:
    def __init__(self, http_response: HTTPResponse):
        self._http_response: HTTPResponse = http_response

    def __repr__(self):
        return f"<{self.__class__.__name__}({bytes(self).decode('utf-8')})>"

    def __str__(self):
        return bytes(self).decode("utf-8")

    def __hash__(self):
        return hash(self._http_response)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response == other._http_response
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response != other._http_response
        else:
            return NotImplemented

    def __len__(self):
        return len(self.response_dict)

    def __getitem__(self, item):
        return self.response_dict.get(item)

    def __bool__(self):
        return len(self) > 0

    def __bytes__(self):
        return self._http_response.read()

    @property
    def response_dict(self) -> dict[str, Any] | list[dict[str, Any]]:
        return loads(bytes(self))

    @property
    def http_response(self):
        return self._http_response


class CustomHTTPResponseChunked:
    def __init__(
            self,
            http_response: HTTPResponse, *,
            chunk: int = CHUNK_SIZE,
            **kwargs):
        if "archive" in kwargs:
            self._archive: Path = validate_path(kwargs.get("archive"), PathType.FILE)
            self._archive.touch(exist_ok=True)
        else:
            self._response_dict: dict[str, Any] | list[dict[str, Any]] = dict()

        self._http_response: HTTPResponse = http_response
        self._chunk: int = chunk
        self.get_response()

    def __str__(self):
        return bytes(self).decode("utf-8")

    def decode_base64(self) -> str:
        return b64decode(str(self["content"])).decode("utf-8")

    def __bytes__(self):
        return self._http_response.read()

    def get_response(self):
        if hasattr(self, "_response_dict"):
            with NamedTemporaryFile(mode="w+b", dir=path_temp) as fb:
                while chunk := self._http_response.read(self._chunk):
                    fb.write(chunk)
                fb.seek(0)
                full_response: dict[str, Any] = loads(fb.read())
                self._response_dict = full_response
        elif hasattr(self, "_archive"):
            with open(self._archive, "wb") as fb:
                while chunk := self._http_response.read(self._chunk):
                    fb.write(chunk)
                fb.seek(0)
        else:
            raise MissingAttributeError

    def __repr__(self):
        return f"<{self.__class__.__name__}({str(self)})>"

    def __hash__(self):
        return hash(bytes(self))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response == other._http_response
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response != other._http_response
        else:
            return NotImplemented

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._response_dict.get(item, None)
        else:
            raise TypeError

    def status(self):
        return self._http_response.getcode()

    @property
    def response_dict(self):
        if hasattr(self, "_response_dict"):
            return self._response_dict
        else:
            return dict()

    @property
    def http_response(self):
        return self._http_response
