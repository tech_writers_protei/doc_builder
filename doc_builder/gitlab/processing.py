# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Any, Mapping

from loguru import logger

from doc_builder.archive.manager import archive_manager
from doc_builder.constants import multi_hasattr, ArchiveFormat
from doc_builder.custom_logger import configure_custom_logging
from doc_builder.errors import InvalidHTTPResponseError
from doc_builder.gitlab.api import ListProjects, ListProjectUsers, ListProjectBranches, GetFileArchive
from doc_builder.gitlab.project import GitProject, UserInfo, BranchInfo
from doc_builder.gitlab.project_dict import git_project_dict


def _get_project(project: Mapping[str, Any]) -> GitProject:
    multi_hasattr(project, "id", "name", "web_url")

    project_id: int = project.get("id")
    project_name: str = project.get("name")
    web_url: str = project.get("web_url")

    return GitProject(project_id, project_name, web_url)


def _get_user_info(user_info: Mapping[str, Any]) -> UserInfo:
    multi_hasattr(user_info, "id", "name", "username")

    user_id: int = user_info.get("id")
    name: str = user_info.get("name")
    username: str = user_info.get("username")

    return UserInfo(user_id, name, username)


def _get_branch_info(branch_info: Mapping[str, Any]) -> BranchInfo:
    multi_hasattr(branch_info, "name", "web_url")

    branch_name: str = branch_info.get("name")
    web_url: str = branch_info.get("web_url")

    return BranchInfo(branch_name, web_url)


def set_projects():
    list_projects: ListProjects = ListProjects()
    _response: dict[str, Any] | list[dict[str, Any]] = list_projects.response.response_dict

    if isinstance(_response, dict):
        git_project: GitProject = _get_project(_response)
        git_project_dict + git_project

    elif isinstance(_response, list):
        git_projects: list[GitProject] = [_get_project(item) for item in _response]
        git_project_dict + git_projects

    else:
        logger.error(
            f"Ответ сервера должен быть типа list[dict] или dict, "
            f"но получен {str(_response)} типа {type(_response)}")
        raise InvalidHTTPResponseError


def set_users():
    v: GitProject
    for v in git_project_dict.values():
        project_id: int = v.project_id
        list_project_users: ListProjectUsers = ListProjectUsers(project_id)

        _response: dict[str, Any] | list[dict[str, Any]] = list_project_users.response.response_dict

        if isinstance(_response, dict):
            user_info: UserInfo = _get_user_info(_response)
            v + user_info

        elif isinstance(_response, list):
            user_infos: list[UserInfo] = [_get_user_info(item) for item in _response]
            v + user_infos

        else:
            logger.error(
                f"Ответ сервера должен быть типа list[dict] или dict, "
                f"но получен {str(_response)} типа {type(_response)}")
            raise InvalidHTTPResponseError


def set_branches():
    v: GitProject
    for v in git_project_dict.values():
        project_id: int = v.project_id
        list_project_branches: ListProjectBranches = ListProjectBranches(project_id)

        _response: dict[str, Any] | list[dict[str, Any]] = list_project_branches.response.response_dict

        if isinstance(_response, dict):
            branch_info: BranchInfo = _get_branch_info(_response)
            v + branch_info

        elif isinstance(_response, list):
            branch_infos: list[BranchInfo] = [_get_branch_info(item) for item in _response]
            v + branch_infos

        else:
            logger.error(
                f"Ответ сервера должен быть типа list[dict] или dict, "
                f"но получен {str(_response)} типа {type(_response)}")
            raise InvalidHTTPResponseError


def set_archive():
    v: GitProject
    for v in git_project_dict.values():
        project_id: int = v.project_id
        archive: Path = Path(__file__).parent.joinpath(v.archive)
        archive.touch(exist_ok=True)

        list_project_branches: GetFileArchive = GetFileArchive(project_id, archive)

        archive_manager.nullify()
        archive_manager.archive = list_project_branches
        archive_manager.unpack()
        logger.info(archive_manager.unpacked_path)


@logger.catch
@configure_custom_logging("doc_builder")
def processing():
    # list_projects: ListProjects = ListProjects()
    # logger.info(f"{list_projects.response.response_dict}")
    # list_project_users: ListProjectUsers = ListProjectUsers(52535581)
    # logger.info(f"{list_project_users.response.response_dict}")
    # list_project_branches: ListProjectBranches = ListProjectBranches(52535581)
    # logger.info(f"{list_project_branches.response.response_dict}")
    archive: Path = Path(__file__).parent.joinpath("file")
    get_file_archive: GetFileArchive = GetFileArchive(52535581, archive, ArchiveFormat.TAR_GZ)
    print(getattr(get_file_archive, "_archive"))


if __name__ == '__main__':
    processing()
