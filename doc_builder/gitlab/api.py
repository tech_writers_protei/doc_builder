# -*- coding: utf-8 -*-
from http.client import HTTPResponse
from json import dumps
from os import environ
from pathlib import Path
from typing import Mapping, Iterable

from loguru import logger

from doc_builder.constants import CustomPort, CustomScheme, CustomMethod, DocumentBranch, SortingOrder, ArchiveFormat
from doc_builder.errors import PrivateTokenMissingError, InvalidEnumValueError
from doc_builder.gitlab.http_handler import CustomHTTPRequest, CustomHTTPResponseChunked, CustomPreparedRequest


def gitlab_api_token() -> str:
    _: str | None = environ.get("GITLAB_API_TOKEN", None)

    if _ is None:
        logger.error(f"No private token")
        raise PrivateTokenMissingError
    else:
        return _


class GitlabAPI:
    def __init__(self, web_hook: str, method: CustomMethod | str = CustomMethod.GET, **kwargs):
        if kwargs is None:
            params: tuple[tuple[str, str], ...] = ()
        else:
            params: tuple[tuple[str, str], ...] = tuple((k, v) for k, v in kwargs.items())

        if isinstance(method, str) and method in CustomMethod.values():
            method: CustomMethod = CustomMethod(method)
        else:
            method: CustomMethod = CustomMethod.GET

        self._web_hook: str = web_hook
        self._params: tuple[tuple[str, str], ...] = params
        self._method: CustomMethod = method
        self._response: CustomHTTPResponseChunked | None = None

        for k, v in kwargs.items():
            setattr(self, k, v)

        self.set_response()

    @property
    def custom_http_request(self):
        host: str = "gitlab.com"
        port: int = CustomPort.HTTPS.value
        scheme: str = CustomScheme.HTTPS.value
        web_hook: str = self._web_hook
        params: tuple[tuple[str, str], ...] = self._params

        return CustomHTTPRequest(
            scheme=scheme,
            web_hook=web_hook,
            host=host,
            port=port,
            params=params)

    def set_response(self):
        method: str = self._method.value

        headers: dict[str, str] = {"PRIVATE-TOKEN": gitlab_api_token()}

        custom_prepared_request: CustomPreparedRequest
        if hasattr(self, "_archive"):
            custom_prepared_request = CustomPreparedRequest.from_custom_request(
                self.custom_http_request,
                method=method,
                headers=headers,
                archive=getattr(self, "_archive"))
        else:
            custom_prepared_request = CustomPreparedRequest.from_custom_request(
                self.custom_http_request,
                method=method,
                headers=headers)

        self._response = custom_prepared_request.http_response_chunked()

    def __bool__(self):
        return self.http_response.status == 200

    @property
    def http_response(self) -> HTTPResponse:
        return self._response.http_response

    def __repr__(self):
        return f"{self.__class__.__name__}: {repr(self._response)}"

    def __str__(self):
        return dumps(self._response.response_dict, indent=2, ensure_ascii=False, sort_keys=True)

    def __getitem__(self, item):
        if isinstance(item, str) and isinstance(self._response.response_dict, Mapping):
            return self._response.response_dict.get(item)
        elif isinstance(item, int) and isinstance(self._response.response_dict, Iterable):
            return self._response.response_dict[item]
        else:
            logger.error(f"Ключ {item} не найден")
            raise KeyError

    def __iter__(self):
        return iter(self._response.response_dict)

    def __hash__(self):
        return hash(self.custom_http_request)

    def __eq__(self, other):
        if isinstance(other, GitlabAPI):
            return self._params == other._params
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, GitlabAPI):
            return self._params == other._params
        else:
            return NotImplemented

    @property
    def response(self):
        return self._response


class ListProjects(GitlabAPI):
    def __init__(self):
        web_hook: str = "api/v4/projects/"
        kwargs: dict[str, str | bool] = {
            "simple": True,
            "membership": True
        }

        super().__init__(web_hook, **kwargs)


class ListRepoTree(GitlabAPI):
    def __init__(self, repo_id: str, branch: str | DocumentBranch):
        if isinstance(branch, DocumentBranch):
            branch: str = branch.value
        elif branch not in DocumentBranch.__members__.values():
            logger.error(f"Указана не существующая ветка {branch}")
            raise InvalidEnumValueError

        web_hook: str = f"api/v4/projects/{repo_id}/repository/tree"
        kwargs: dict[str, str | bool] = {
            "recursive": True,
            "ref": branch
        }

        super().__init__(web_hook, **kwargs)


class FindRepoByName(GitlabAPI):
    def __init__(self, search: str, sorting_order: str | SortingOrder):
        if isinstance(sorting_order, SortingOrder):
            sorting_order: str = sorting_order.value

        elif sorting_order not in SortingOrder.__members__.values():
            logger.error(f"Указана некорректный тип сортировки {sorting_order}")
            raise InvalidEnumValueError

        web_hook: str = f"api/v4/projects"
        kwargs: dict[str, str | bool] = {
            "search": search,
            "sort": sorting_order
        }

        super().__init__(web_hook, **kwargs)


class ListProjectUsers(GitlabAPI):
    def __init__(self, project_id: str | int):
        web_hook: str = f"api/v4/projects/{project_id}/users"
        kwargs: dict[str, str | bool] = dict()

        super().__init__(web_hook, **kwargs)


class ListProjectBranches(GitlabAPI):
    def __init__(self, project_id: str | int):
        web_hook: str = f"api/v4/projects/{project_id}/repository/branches"
        kwargs: dict[str, str | bool] = dict()

        super().__init__(web_hook, **kwargs)


class GetRepositoryFile(GitlabAPI):
    def __init__(self, project_id: str | int, file_path: str, branch: str | DocumentBranch):
        if isinstance(branch, DocumentBranch):
            branch: str = branch.value
        elif branch not in DocumentBranch.__members__.values():
            logger.error(f"Указана не существующая ветка {branch}")
            raise InvalidEnumValueError

        kwargs: dict[str, str] = {"ref": branch}
        web_hook: str = f"api/v4/projects/{project_id}/repository/files/{file_path}"

        super().__init__(web_hook, **kwargs)


class GetFileArchive(GitlabAPI):
    def __init__(
            self,
            project_id: str | int,
            archive: str | Path,
            archive_format: str | ArchiveFormat = ArchiveFormat.TAR_GZ):
        if isinstance(archive_format, str):
            archive_format: ArchiveFormat = ArchiveFormat[archive_format]

        web_hook: str = f"api/v4/projects/{project_id}/repository/archive{archive_format.value}"
        kwargs: dict[str, str | bool] = {"_archive": archive}

        super().__init__(web_hook, **kwargs)
