# -*- coding: utf-8 -*-
from json import dumps

from loguru import logger

from doc_builder.gitlab.http_handler import CustomHTTPResponse, CustomHTTPResponseChunked


class GitlabAPIParser:
    def __init__(self):
        self._response: CustomHTTPResponse | CustomHTTPResponseChunked | None = None

    def __str__(self):
        return dumps(self._response.response_dict, indent=2, ensure_ascii=False, sort_keys=True)

    def __repr__(self):
        return f"{self.__class__.__name__}: {repr(self._response)}"

    def __bool__(self):
        return self.http_response.status == 200

    @property
    def http_response(self):
        return self._response.http_response

    def check_status(self) -> bool:
        if not bool(self):
            logger.error(f"{self.http_response.status}, {self.http_response.reason}")
            return False
        else:
            return True

    def clear(self):
        self._response = None

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, value):
        if isinstance(value, (CustomHTTPResponseChunked, CustomHTTPResponse)):
            self._response = value
        else:
            raise ValueError

    def __iter__(self):
        return iter(self._response.response_dict)

    def __getitem__(self, item):
        if isinstance(self._response.response_dict, dict):
            if isinstance(item, str):
                return self._response.response_dict.get(item, None)
            else:
                logger.error(f"Dictionary does not have a key {item}")
                raise KeyError
        elif isinstance(self._response.response_dict, list):
            if isinstance(item, int) and 0 <= item < len(self._response.response_dict):
                return self._response.response_dict[item]
            else:
                logger.error(f"List does not have a key {item}")
                raise ValueError
        else:
            raise TypeError


gitlab_api_parser: GitlabAPIParser = GitlabAPIParser()
