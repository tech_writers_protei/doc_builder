# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable, Iterator, NamedTuple

from loguru import logger

from doc_builder.constants import validate_path, PathType, multi_non_none


class UserInfo(NamedTuple):
    user_id: int
    name: str
    username: str

    def __str__(self):
        return (
            f"<{self.__class__.__name__}> "
            f"user_id = {self.user_id}, "
            f"name = {self.name}, "
            f"username = {self.username}")

    __repr__ = __str__

    def __hash__(self):
        return hash(self.user_id)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.user_id == other.user_id
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.user_id != other.user_id
        else:
            return NotImplemented


class BranchInfo(NamedTuple):
    branch_name: str
    web_url: str

    def __str__(self):
        return (
            f"<{self.__class__.__name__}> "
            f"branch_name = {self.branch_name}, "
            f"web_url = {self.web_url}")

    __repr__ = __str__

    def __hash__(self):
        return hash(self.branch_name)


class GitProject:
    def __init__(
            self,
            project_id: int,
            project_name: str,
            web_url: str,
            archive: str | Path | None = None,
            branches: Iterable[BranchInfo] = None,
            users: Iterable[UserInfo] = None):
        multi_non_none(project_id, project_name, web_url)

        if branches is None:
            branches: set[BranchInfo] = set()
        if users is None:
            users: set[UserInfo] = set()

        self._project_id: int = project_id
        self._project_name: str = project_name
        self._web_url: str = web_url
        self._branches: set[BranchInfo] = {*branches}
        self._users: set[UserInfo] = {*users}

        if archive is None:
            self._archive: str | Path | None = None
        else:
            self._archive: str | Path | None = validate_path(archive, PathType.DIRECTORY)

    def __str__(self):
        if self._branches:
            _branches_str: str = "\n".join(str(branch) for branch in self._branches)
            branches_str: str = f"Branches:\n{_branches_str}"
        else:
            branches_str: str = "Branches: None"
        if self._users:
            _users_str: str = "\n".join(str(user) for user in self._users)
            users_str: str = f"Users:\n{_users_str}"
        else:
            users_str: str = "Users: None"
        return (
            f"\n<{self.__class__.__name__}> "
            f"project_id = {self._project_id}, "
            f"project_name = {self._project_name}, "
            f"web_url = {self._web_url}"
            f"\n{branches_str}\n{users_str}\n")

    __repr__ = __str__

    def __hash__(self):
        return hash(self._project_id)

    def __add__(self, other):
        if isinstance(other, BranchInfo):
            self._branches.add(other)
        elif isinstance(other, UserInfo):
            self._users.add(other)
        elif isinstance(other, Iterable):
            branches: tuple[BranchInfo, ...] = tuple(item for item in other if isinstance(item, BranchInfo))
            users: tuple[UserInfo, ...] = tuple(item for item in other if isinstance(item, UserInfo))
            self._branches.update(branches)
            self._users.update(users)
        else:
            logger.error(f"Элемент {other} типа {type(other)} невозможно добавить")

    def __sub__(self, other):
        try:
            if isinstance(other, BranchInfo):
                self._branches.remove(other)
            elif isinstance(other, UserInfo):
                self._users.remove(other)
            else:
                logger.error(f"Элемент {other} типа {type(other)} невозможно удалить")
        except KeyError:
            logger.error(f"Элемент {other} типа {type(other)} не найден")

    def iter_branches(self) -> Iterator[BranchInfo]:
        return iter(self._branches)

    def iter_users(self) -> Iterator[UserInfo]:
        return iter(self._users)

    @property
    def archive(self):
        return self._archive

    @archive.setter
    def archive(self, value):
        self._archive = value

    @property
    def project_name(self):
        return self._project_name

    @property
    def project_id(self):
        return self._project_id
