# -*- coding: utf-8 -*-
from typing import Iterable

from loguru import logger

from doc_builder.gitlab.project import GitProject


class GitProjectDict(dict):
    def __init__(self, git_projects: Iterable[GitProject] = None):
        if git_projects is None:
            git_projects: list[GitProject] = []
        kwargs: dict[str, GitProject] = {
            git_project.project_name: git_project for git_project in git_projects}
        super().__init__(**kwargs)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self.keys():
                return self.get(item)
            else:
                raise ValueError
        elif isinstance(item, int):
            value: GitProject
            for value in self.values():
                if value.project_id == item:
                    return value
            else:
                raise ValueError
        else:
            raise KeyError

    @property
    def project_ids(self) -> tuple[int, ...]:
        _: GitProject
        return tuple(_.project_id for _ in self.values())

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.keys()
        elif isinstance(item, int):
            return item in self.project_ids
        else:
            return False

    def __add__(self, other):
        if isinstance(other, GitProject):
            self[other.project_name] = other
        elif isinstance(other, Iterable):
            projects: dict[str, GitProject] = {
                project.project_name: project for project in other if isinstance(project, GitProject)}
            self.update(**projects)
        else:
            logger.error(f"Элемент {other} должен быть типа GitProject, но получено {type(other)}")


git_project_dict: GitProjectDict = GitProjectDict()
