# -*- coding: utf-8 -*-
from json import load
from pathlib import Path
from typing import TypeAlias

from doc_builder.constants import validate_path, PathType
from doc_builder.gitlab.project import GitProject, BranchInfo, UserInfo

DictGit: TypeAlias = dict[str, str | int | dict[str, str | int]] | list[dict[str, str | int | dict[str, str | int]]]


class GitProjectReader:
    def __init__(self):
        self._path: Path | None = None
        self._content: list[dict[str, str | int | dict[str, str | int]]] = []

    def path(self, path):
        self._path = validate_path(path, PathType.FILE)

    def read(self):
        with open(self._path, "r") as f:
            _: DictGit = load(f)
        self._content.extend(_)

    def __str__(self):
        return f"{self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __iter__(self):
        return iter(self._content)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def git_projects(self):
        git_projects: list[GitProject] = []

        for element in iter(self):
            for project in element.values():
                project_id: int = project.get("project_id")
                project_name: str = project.get("project_name")
                web_url: str = project.get("web_url")
                archive: str = project.get("archive")
                branches: list[BranchInfo] = []
                users: list[UserInfo] = []

                for branch in project.get("branches"):
                    branch_name: str = branch.get("branch_name")
                    _web_url: str = branch.get("web_url")

                    branch_info: BranchInfo = BranchInfo(branch_name, _web_url)
                    branches.append(branch_info)

                for user in project.get("users"):
                    user_id: int = user.get("user_id")
                    name: str = user.get("name")
                    username: str = user.get("username")

                    user_info: UserInfo = UserInfo(user_id, name, username)
                    users.append(user_info)

                git_project: GitProject = GitProject(project_id, project_name, web_url, archive, branches, users)

                git_projects.append(git_project)

        return git_projects
