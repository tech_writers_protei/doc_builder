# -*- coding: utf-8 -*-
from json import dump
from pathlib import Path
from typing import Iterable

from loguru import logger

from doc_builder.constants import PathType, validate_path
from doc_builder.gitlab.project import GitProject


class GitProjectWriter:
    def __init__(self, path: str | Path, git_projects: Iterable[GitProject] = None):
        if git_projects is None:
            git_projects: list[GitProject] = []

        self._path: Path = validate_path(path, PathType.FILE)
        self._git_projects: list[GitProject] = [*git_projects]

    def __str__(self):
        return f"{self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def to_object(self):
        return {"projects": self._git_projects}

    def write(self):
        with open(self._path, "w") as f:
            dump(self.to_object(), f)
        logger.info(f"File {self._path} is saved")
