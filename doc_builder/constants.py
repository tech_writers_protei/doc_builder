# -*- coding: utf-8 -*-
from enum import Enum
from os import environ
from pathlib import Path
from typing import Mapping, Sequence, Any

from loguru import logger

from doc_builder.errors import MissingPathTypeError, MissingPathError, InvalidPathTypeError, MissingAttributeError, \
    NoneValueError

path_temp: Path = Path(__file__).parent
terms_log_folder: Path = Path.home().joinpath("Desktop").joinpath("_doc_builder_logs")


try:
    PROTOCOL: str = environ.get("PROTOCOL")
except KeyError:
    PROTOCOL: str = "HTTPS"

try:
    _: str = environ.get("CHUNK_SIZE")
    CHUNK_SIZE: int = int(_)
except KeyError and TypeError:
    CHUNK_SIZE: int = 4096


def iter_getitem(mapping: Mapping, keys: Sequence[str]):
    if len(keys) == 1:
        return mapping.get(keys[0])
    else:
        return iter_getitem(mapping.get(keys[0]), keys[1:])


class CustomPort(Enum):
    HTTP = 80
    HTTPS = 443

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class CustomScheme(Enum):
    HTTP = "http"
    HTTPS = "https"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class CustomMethod(Enum):
    GET = "GET"
    POST = "POST"
    DELETE = "DELETE"
    PUT = "PUT"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"

    @classmethod
    def _missing_(cls, value: object):
        return None

    @classmethod
    def values(cls):
        return cls.__members__.values()


class DocumentBranch(Enum):
    MAIN = "main"
    MASTER = "master"
    PAGES = "pages"
    DEVELOP_PAGES = "develop_pages"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class DocumentType(Enum):
    ADMIN_GUIDE = "Руководство администратора"
    USER_GUIDE = "Руководство пользователя"
    TECH_DESCRIPTION = "Техническое описание"
    DOCUMENT_TYPE = "DocumentType"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class DocumentFormat(Enum):
    HTML = "html"
    PDF = "pdf"
    DOCX = "docx"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class SortingOrder(Enum):
    ASC = "asc"
    DESC = "desc"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class DocumentLanguage(Enum):
    RU = "ru"
    EN = "en"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class ArchiveFormat(Enum):
    BZ2 = ".bz2"
    TAR = ".tar"
    TAR_BZ2 = ".tar.bz2"
    TAR_GZ = ".tar.gz"
    TB2 = ".tb2"
    TBZ = ".tbz"
    TBZ2 = ".tbz2"
    ZIP = ".zip"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"

    @classmethod
    def _missing_(cls, value: object) -> Any:
        logger.info(f"Формат {value} не обнаружен")
        return cls.TAR_GZ


class PathType(Enum):
    FILE = "file"
    DIRECTORY = "directory"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"

    @classmethod
    def _missing_(cls, value: object) -> Any:
        logger.info(f"Формат {value} не обнаружен")
        raise MissingPathTypeError


class FileType(Enum):
    HTML = "html"
    PDF = "pdf"
    DOCX = "docx"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


def validate_path(path: str | Path, path_type: str | PathType) -> Path:
    if isinstance(path_type, str):
        path_type: PathType = PathType(path_type)

    if isinstance(path, str):
        path: Path = Path(path)

    if not path.exists():
        logger.error(f"Путь {path} не существует")
        raise MissingPathError

    elif path_type == PathType.FILE and path.is_dir():
        logger.error(f"Путь {path} должен указывать на файл, но указывает на директорию")
        raise InvalidPathTypeError

    elif path_type == PathType.DIRECTORY and path.is_file():
        logger.error(f"Путь {path} должен указывать на директорию, но указывает на файл")
        raise InvalidPathTypeError

    else:
        return path


def multi_hasattr(value: Mapping[str, Any], *attributes: str):
    if any(not hasattr(value, attribute) for attribute in attributes):
        raise MissingAttributeError
    return


def multi_non_none(*values):
    if values is None or any(value is None for value in values):
        raise NoneValueError
    return
