# -*- coding: utf-8 -*-
from collections import Counter
from typing import Any, Iterable

from loguru import logger

from doc_builder.ascii_doc.basic import adoc_basic
from doc_builder.errors import MissingAsciiDocEnumValuesError, InvalidAsciiDocAttributeLineError


def is_asciidoc_attribute(line: str):
    line: str = line.strip()
    counter: Counter = Counter(line)

    return line.startswith(":") or counter.get(":") != 2


class AsciiDocAttribute:
    def __init__(self, name: str, available: Iterable[Any] = None):
        if available is None:
            available: list[Any] = []

        self._name: str = name
        self._value: Any = None
        self._available: tuple[Any, ...] = *available,

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    def __str__(self):
        return f"{self._name} {self._value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(name: {self._name}, value: {self._value}>"

    @classmethod
    def from_string(cls, line: str):
        if not is_asciidoc_attribute(line):
            logger.error(f"Строка {line} должна начинаться с :")
            logger.error(f"В строке {line} должно быть два :")
            raise InvalidAsciiDocAttributeLineError

        line: str = line.strip().removeprefix(":")

        if line.endswith(":"):
            name: str = line.removeprefix("!").removesuffix(":")
            value: bool = line.startswith("!")

        else:
            name, value = line.rsplit(":", 1)

        ascii_doc_attribute = cls(name)
        ascii_doc_attribute._value = value

        return ascii_doc_attribute

    @property
    def name(self):
        return self._name

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self._name == other._name:
                return self._value == other._value
            else:
                return False
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            if self._name != other._name:
                return True
            else:
                return self._value != other._value
        else:
            return NotImplemented

    def __contains__(self, item):
        if self._available is None or self._available == []:
            return True
        else:
            return item in self._available


class AsciiDocEnumAttribute(AsciiDocAttribute):
    def __init__(self, name: str, available: Iterable[Any] = None):
        if available is None:
            logger.error(f"Значения Enum не заданы")
            raise MissingAsciiDocEnumValuesError

        super().__init__(name, available)

    def __str__(self):
        return f":{self._name}: {self._value}"

    def __contains__(self, item):
        return item in self._available


class AsciiDocFlagAttribute(AsciiDocEnumAttribute):
    def __init__(self, name: str):
        available: tuple[bool, ...] = (True, False)
        super().__init__(name, available)

    def __str__(self):
        if self._value:
            return f":{self._name}:"
        else:
            return f":!{self._name}:"


def convert_asciidoc_attribute(adoc_attribute: AsciiDocAttribute):
    name: str = adoc_attribute.name

    if adoc_basic.is_attr(name) or name not in adoc_basic:
        return adoc_attribute
    elif adoc_basic.is_flag(name):
        _adoc_attribute: AsciiDocFlagAttribute = adoc_basic.get(name)
    elif adoc_basic.is_enum(name):
        _adoc_attribute: AsciiDocEnumAttribute = adoc_basic.get(name)
    else:
        raise ValueError

    _adoc_attribute.value = adoc_attribute.value
    return _adoc_attribute
