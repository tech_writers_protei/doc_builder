# -*- coding: utf-8 -*-
from loguru import logger

from doc_builder.ascii_doc.attribute import AsciiDocAttribute, AsciiDocFlagAttribute, \
    AsciiDocEnumAttribute


sectnums: AsciiDocFlagAttribute = AsciiDocFlagAttribute("sectnums")
sectlinks: AsciiDocFlagAttribute = AsciiDocFlagAttribute("sectlinks")
pagenums: AsciiDocFlagAttribute = AsciiDocFlagAttribute("pagenums")
experimental: AsciiDocFlagAttribute = AsciiDocFlagAttribute("experimental")
titlepage: AsciiDocFlagAttribute = AsciiDocFlagAttribute("titlepage")
sectanchors: AsciiDocFlagAttribute = AsciiDocFlagAttribute("sectanchors")
kroki_fetch_diagram: AsciiDocFlagAttribute = AsciiDocFlagAttribute("kroki-fetch-diagram")
nofooter: AsciiDocFlagAttribute = AsciiDocFlagAttribute("nofooter")
noheader: AsciiDocFlagAttribute = AsciiDocFlagAttribute("noheader")
sectids: AsciiDocFlagAttribute = AsciiDocFlagAttribute("sectids")


icons: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "icons",
    ("font", "image"))
doctype: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "doctype",
    ("article", "book", "manpage", "inline"))
sectnumlevels: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "sectnumlevels",
    (1, 2, 3, 4, 5))
toclevels: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "toclevels",
    (1, 2, 3, 4, 5))
attribute_missing: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "attribute-missing",
    ("skip", "drop", "drop-line", "warn"))
leveloffset: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "leveloffset",
    (+1, +2, +3, +4, +5, -1, -2, -3, -4, -5))
pdf_page_layout: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "pdf-page-layout",
    ("portrait", "landscape"))
source_highlighter: AsciiDocEnumAttribute = AsciiDocEnumAttribute(
    "source-highlighter",
    ("rouge", "coderay", "highlightjs", "prettify", "pygments"))


chapter_label: AsciiDocAttribute = AsciiDocAttribute("chapter-label")
figure_caption: AsciiDocAttribute = AsciiDocAttribute("figure-caption")
image_caption: AsciiDocAttribute = AsciiDocAttribute("image-caption")
example_caption: AsciiDocAttribute = AsciiDocAttribute("example-caption")
table_caption: AsciiDocAttribute = AsciiDocAttribute("table-caption")
tip_caption: AsciiDocAttribute = AsciiDocAttribute("tip-caption")
important_caption: AsciiDocAttribute = AsciiDocAttribute("important-caption")
caution_caption: AsciiDocAttribute = AsciiDocAttribute("caution-caption")
note_caption: AsciiDocAttribute = AsciiDocAttribute("note-caption")
warning_caption: AsciiDocAttribute = AsciiDocAttribute("warning-caption")
pdf_fontsdir: AsciiDocAttribute = AsciiDocAttribute("pdf-fontsdir")
pdf_themesdir: AsciiDocAttribute = AsciiDocAttribute("pdf-themesdir")
pdf_stylesdir: AsciiDocAttribute = AsciiDocAttribute("pdf-stylesdir")
stylesdir: AsciiDocAttribute = AsciiDocAttribute("stylesdir")
description: AsciiDocAttribute = AsciiDocAttribute("description")
stylesheet: AsciiDocAttribute = AsciiDocAttribute("stylesheet")
toc_title: AsciiDocAttribute = AsciiDocAttribute("toc-title")
asciidoctorconfig: AsciiDocAttribute = AsciiDocAttribute("asciidoctorconfig")


flag_attributes: tuple[AsciiDocFlagAttribute, ...] = (
    sectnums,
    sectlinks,
    pagenums,
    experimental,
    titlepage,
    sectanchors,
    kroki_fetch_diagram,
    nofooter,
    noheader,
    sectids
)

enum_attributes: tuple[AsciiDocEnumAttribute, ...] = (
    icons,
    doctype,
    sectnumlevels,
    toclevels,
    attribute_missing,
    leveloffset,
    pdf_page_layout,
    source_highlighter
)

attributes: tuple[AsciiDocAttribute, ...] = (
    chapter_label,
    figure_caption,
    image_caption,
    example_caption,
    table_caption,
    tip_caption,
    important_caption,
    caution_caption,
    note_caption,
    warning_caption,
    pdf_fontsdir,
    pdf_themesdir,
    pdf_stylesdir,
    stylesdir,
    description,
    stylesheet,
    toc_title,
    asciidoctorconfig
)


class AsciiDocBasic(dict):
    def __init__(self):
        super().__init__()
        self._flags: set[str] = set()
        self._enums: set[str] = set()
        self._attrs: set[str] = set()
        self.register_attributes()

    def is_flag(self, name: str) -> bool:
        return name in self._flags

    def is_enum(self, name: str) -> bool:
        return name in self._enums

    def is_attr(self, name: str) -> bool:
        return name in self._attrs

    def register_attributes(self):
        attribute: AsciiDocAttribute

        for attribute in flag_attributes:
            self._flags.add(attribute.name)
            self[attribute.name] = attribute

        for attribute in enum_attributes:
            self._enums.add(attribute.name)
            self[attribute.name] = attribute

        for attribute in attributes:
            self._attrs.add(attribute.name)
            self[attribute.name] = attribute

    def __getitem__(self, item):
        if isinstance(item, str):
            return self.get(item.replace("_", "-"))
        else:
            logger.error(f"Ключ {item} типа {type(item)} не найден")

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.keys()
        else:
            return NotImplemented

    def validate_item(self, item: AsciiDocAttribute) -> bool:
        _key: str = item.name

        if _key not in self.keys():
            return False

        ascii_doc_attribute: AsciiDocAttribute = self.get(_key)

        if isinstance(ascii_doc_attribute, AsciiDocEnumAttribute):
            return item.value in ascii_doc_attribute
        else:
            return True


adoc_basic: AsciiDocBasic = AsciiDocBasic()
