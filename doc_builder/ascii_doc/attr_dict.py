# -*- coding: utf-8 -*-
from typing import Iterable

from loguru import logger

from doc_builder.ascii_doc.attribute import AsciiDocAttribute
from doc_builder.ascii_doc.basic import AsciiDocBasic, adoc_basic


class AsciiDocAttrDict(dict):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._adoc_basic: AsciiDocBasic = adoc_basic

    @property
    def _items_str(self) -> tuple[str, ...]:
        return tuple(f"{k} = {v}" for k, v in self.items())

    def __str__(self):
        return ", ".join(self._items_str)

    def __repr__(self):
        _: str = "\n".join(self._items_str)
        return f"<{self.__class__.__name__}>:\n{_}"

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.keys()
        elif isinstance(item, AsciiDocAttribute):
            return item in self.values()
        else:
            return NotImplemented

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, AsciiDocAttribute):
            self[other.name] = other
        elif isinstance(other, Iterable):
            _: list[AsciiDocAttribute] = list(
                filter(lambda x: isinstance(x, AsciiDocAttribute), other))
            asciidoc_attributes = {
                asciidoc_attribute.name: asciidoc_attribute
                for asciidoc_attribute in _}
            self.update(asciidoc_attributes)
        else:
            logger.error(f"Элемент {other} типа {type(other)} невозможно добавить")

    def is_basic(self, ascii_doc_attribute: AsciiDocAttribute) -> bool:
        return ascii_doc_attribute in self._adoc_basic


adoc_attr_dict: AsciiDocAttrDict = AsciiDocAttrDict()
