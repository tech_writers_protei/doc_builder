# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable, NamedTuple, Iterator

from loguru import logger

from doc_builder.ascii_doc.attribute import is_asciidoc_attribute, AsciiDocAttribute, convert_asciidoc_attribute
from doc_builder.ascii_doc.file import AsciiDocFile
from doc_builder.errors import InvalidIncludeFileError


class IncludeFileItem(NamedTuple):
    line: str
    level: int

    def __str__(self):
        return (
            f"\n\n:leveloffset: +{self.level}"
            f"\n\ninclude::{self.line}[]"
            f"\n\n:leveloffset: -{self.level}")

    def __repr__(self):
        return f"<{self.__class__.__name__}(\n{self.line}, {self.level})>"

    def __hash__(self):
        return hash((self.line, self.level))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.line, self.level == other.line, other.level
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.line, self.level != other.line, other.level
        else:
            return NotImplemented


class AsciiDocIncludeFile(AsciiDocFile):
    def __init__(self, path: str | Path, include_files: Iterable[IncludeFileItem] = None):
        super().__init__(path)

        if include_files is None:
            include_files: list[IncludeFileItem] = []

        self._include_files: list[IncludeFileItem] = [*include_files]
        self._asciidoc_attributes: list[AsciiDocAttribute] = []

    def iter_include(self) -> Iterator[IncludeFileItem]:
        return iter(self._include_files)

    def __str__(self):
        _lines: list[str] = [str(include_file) for include_file in iter(self)]
        return "\n".join(_lines)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    @property
    def files(self) -> list[str]:
        return [item.line for item in iter(self)]

    def __add__(self, other):
        if isinstance(other, str):
            self._content.append(other)
        elif isinstance(other, IncludeFileItem):
            self._include_files.append(other)
        elif isinstance(other, Iterable):
            _include_files: list[IncludeFileItem] = [
                include_file for include_file in other if isinstance(include_file, IncludeFileItem)]
            _lines: list[str] = [line for line in other if isinstance(line, str)]
            self._include_files.extend(_include_files)
            self._content.extend(_lines)
        else:
            logger.error(f"Элемент {other} должен быть типа IncludeFileItem, но получено {type(other)}")

    def insert_asciidoc_attribute(self, line: str, index: int):
        self._content.insert(index, line)

        if is_asciidoc_attribute(line):
            _: AsciiDocAttribute = AsciiDocAttribute.from_string(line)
            asciidoc_attribute = convert_asciidoc_attribute(_)
            self._asciidoc_attributes.append(asciidoc_attribute)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._include_files[item]

        elif isinstance(item, str):
            if item not in self.files:
                logger.error(f"Файл {item} не найден")
                raise InvalidIncludeFileError

            for file in iter(self):
                if file.line == item:
                    return file

                else:
                    continue

        else:
            raise KeyError

    def generate(self):
        _: list[str] = [str(include_file) for include_file in self._include_files]
        self._content = _

    def read(self):
        try:
            with open(self._path, "r") as f:
                _content: list[str] = f.readlines()
            _offset_plus: list[int] = [
                index for index, _line in enumerate(_content)
                if _line.startswith(":leveloffset: +")
            ]
            _lines: list[int] = [
                index for index, _line in enumerate(_content) if _line.startswith("include_files::")
            ]
            for index_level, index_line in zip(_offset_plus, _lines):
                line: str = _content[index_line].removeprefix("include_files::").removesuffix("[]")
                level: int = int(_content[index_level].removeprefix(":leveloffset: +"))
                include_file: IncludeFileItem = IncludeFileItem(line, level)
                self + include_file
        except FileNotFoundError:
            logger.error(f"Файл {self._path} не найден")
            raise
        except PermissionError:
            logger.error(f"Недостаточно прав для чтения файла {self._path}")
            raise
        except OSError as e:
            logger.error(f"{e.__class__.__name__}: {e.strerror}")
            raise

    def write(self):
        if not self._path.exists():
            self._path.touch(exist_ok=True)
        try:
            with open(self._path, "w") as f:
                f.write(str(self))
        except PermissionError:
            logger.error(f"Недостаточно прав для записи файла {self._path}")
            raise
        except OSError as e:
            logger.error(f"{e.__class__.__name__}: {e.strerror}")
            raise
