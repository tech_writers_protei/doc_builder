# -*- coding: utf-8 -*-
from typing import Any, Iterable, NamedTuple

from loguru import logger

from doc_builder.errors import InvalidAttributeError


_front_matter_required: tuple[str, ...] = (
    "title", "description", "weight", "type"
)


def not_none_values(obj: Any, attrs: Iterable[str]) -> dict[str, Any]:
    return {
        attr: getattr(obj, attr)
        for attr in attrs
        if getattr(obj, attr) is not None
    }


def stringify(value: Any):
    if value is None:
        return
    elif isinstance(value, str):
        return f'"{value}"'
    elif isinstance(value, int):
        return f"{value}"
    else:
        return NotImplemented


class FrontMatter(NamedTuple):
    title: str
    description: str | None
    weight: int | None
    type: str | None = "docs"
    menu: str | None = None
    draft: bool | None = False

    def __str__(self):
        _sep: str = "---"
        _str_values: tuple[str, ...] = tuple(
            f"{k}: {stringify(v)}"
            for k, v in not_none_values(self, self._fields).items())
        return "\n".join((_sep, *_str_values, _sep))

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def has_attr(self, attr: str):
        if attr not in _front_matter_required:
            logger.error(f"Атрибут {attr} не найден")
            raise InvalidAttributeError

        return getattr(self, attr) is not None

    def __bool__(self):
        return self.weight is not None

    def __int__(self):
        if bool(self):
            return self.weight
        else:
            return 1_000

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.title == other.title
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.title != other.title
        else:
            return NotImplemented
