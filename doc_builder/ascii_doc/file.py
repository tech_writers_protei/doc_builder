# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Any, Iterable, Sequence

from loguru import logger

from doc_builder.ascii_doc.attr_dict import AsciiDocAttrDict, adoc_attr_dict
from doc_builder.ascii_doc.attribute import AsciiDocAttribute, convert_asciidoc_attribute
from doc_builder.ascii_doc.front_matter import FrontMatter
from doc_builder.constants import validate_path, PathType
from doc_builder.errors import InvalidFrontMatterLineError, MissingFrontMatterError, InvalidFileStructureError, \
    InvalidFrontMatterError, InvalidFrontMatterEntityError

_REQUIRED_PARTS: tuple[str, ...] = ("content", "common")
_BOOL: tuple[str | None, ...] = ("true", "false", None)


def process_line(line: str) -> str:
    return line.removesuffix("\n").strip()


def parse_front_matter(line: str) -> tuple[str, str]:
    if ":" not in line:
        logger.error(f"Line {line} does not have a colon")
        raise InvalidFrontMatterLineError

    _: list[str] = line.split(":")
    key: str = _[0].strip()
    value: str = _[1].strip()
    return key, value


def intify(value: str | None) -> int | None:
    if value is None:
        return value
    elif not value.isnumeric():
        logger.error(f"Value {value} is not numeric")
        return None
    else:
        return int(value)


def boolify(value: str | None) -> bool:
    if value == "true":
        return True
    elif value not in _BOOL:
        logger.error(f"Value {value} is not boolean")
        return False
    else:
        return False


def remove_from_list(values: Sequence[str] = None, indexes: Iterable[int] = None):
    if isinstance(values, str):
        logger.error(f"Получено str вместо Sequence")
        raise TypeError

    if values is None:
        logger.error(f"Список пуст")
        raise ValueError
    else:
        values: list[str] = [*values]

    if indexes is None:
        return values

    if any(index > len(values) for index in indexes):
        logger.error(f"Максимальный индекс превышает длину списка")
        raise ValueError

    for idx in reversed(sorted(indexes)):
        values.pop(idx)

    return values


class AsciiDocFile:
    def __init__(self, path: str | Path):
        self._path: Path = validate_path(path, PathType.FILE)
        self._front_matter: FrontMatter | None = None
        self._ascii_doc_attribute_dict: AsciiDocAttrDict = adoc_attr_dict
        self._content: list[str] = []
        self._is_empty: bool | None = None

    def read(self):
        with open(self._path, "r", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def __iter__(self):
        return iter(self._content)

    def process(self):
        indexes: list[int] = [
            index for index, line in enumerate(iter(self))
            if process_line(line) == "---"]

        if not indexes:
            logger.error(f"Файл {self._path} пуст")
            raise MissingFrontMatterError

        if len(indexes) < 2:
            logger.error(f"Количество строк '---' должно быть 2, но найдено {len(indexes)}")
            logger.error(f"Файл {self._path}")
            raise InvalidFileStructureError

        if indexes[0]:
            logger.error(f"Самая первая строка должна быть '---', но найдено {self._content[0]}")
            raise InvalidFileStructureError

    def set_empty(self):
        self._is_empty = not any(process_line(self._content[index]) for index in range(6, len(self)))

    def set_ascii_doc_attr_dict(self):
        _indexes: list[int] = []

        for index, line in enumerate(iter(self)):
            if line.startswith(":") and ":" in line.removeprefix(":"):
                _indexes.append(index)

                ascii_doc_attribute: AsciiDocAttribute = AsciiDocAttribute.from_string(line)
                self._ascii_doc_attribute_dict + convert_asciidoc_attribute(ascii_doc_attribute)

        self._content = remove_from_list(self._content, _indexes)

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __bool__(self):
        return self._front_matter is not None

    def __len__(self):
        return len(self._content)

    def __int__(self):
        if not bool(self):
            logger.error(f"Front matter не задано")
            logger.error(f"file = {self._path}")
            raise InvalidFrontMatterError
        else:
            return int(self._front_matter)

    def __hash__(self):
        return hash(self._path)

    def __key(self):
        return self.parent, int(self), self.name

    @property
    def parent(self):
        return self._path.parent

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name <= other.name
            else:
                return int(self) < int(other)
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name >= other.name
            else:
                return int(self) > int(other)
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name < other.name
            else:
                return int(self) < int(other)
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            if int(self) == int(other):
                return self.name > other.name
            else:
                return int(self) > int(other)
        else:
            return NotImplemented

    @property
    def name(self) -> str:
        return self._path.stem

    def set_front_matter(self):
        _indexes: list[int] = []
        front_matter_dict: dict[str, Any] = dict()

        for index, line in enumerate(iter(self)):
            if not line.removesuffix("\n"):
                continue
            else:
                _indexes.append(index)

                key, value = parse_front_matter(line)
                if key in FrontMatter._fields:
                    front_matter_dict[key] = value

        if "title" not in front_matter_dict:
            logger.error(f"Ключ 'title' должен быть в front matter")
            raise InvalidFrontMatterEntityError

        _title: str = front_matter_dict.get("title")
        _description: str | None = front_matter_dict.get("description", None)
        _weight: int | None = front_matter_dict.get("weight", None)
        _menu: str | None = front_matter_dict.get("menu", None)
        _type: str | None = front_matter_dict.get("type", None)
        _draft: bool | None = front_matter_dict.get("draft", None)

        self._front_matter = FrontMatter(
            _title.strip("\""),
            _description.strip("\""),
            intify(_weight),
            _menu,
            _type,
            boolify(_draft))

        self._content = remove_from_list(self._content, _indexes)

    @property
    def front_matter(self):
        return self._front_matter

    @property
    def path(self):
        return self._path

    @property
    def is_empty(self):
        return self._is_empty

    def __add__(self, other):
        if isinstance(other, str):
            self._content.append(other)
        else:
            return NotImplemented
