# -*- coding: utf-8 -*-
from enum import Enum
from subprocess import run, CompletedProcess, CalledProcessError
from typing import Mapping, NamedTuple, Iterable

from loguru import logger

from doc_builder.errors import InvalidAddedCommandItemError


def attributify(value: str) -> str:
    return f"-a {value}"


def stringify_iterable(values: Iterable[str]) -> str:
    return " ".join(values)


def run_command(command: str):
    try:
        _completed_process: CompletedProcess = run(
            command,
            shell=True,
            capture_output=True,
            check=True,
            text=True)
    except CalledProcessError as e:
        logger.error(f"Команда {e.cmd} с атрибутами {e.args}, код {e.returncode}")
        raise
    except OSError as e:
        logger.error(f"Ошибка {e.__class__.__name__}({e.errno}): {e.strerror}\nКоманда: {command}")
        raise
    else:
        stdout: str | None = _completed_process.stdout
        stderr: str | None = _completed_process.stdout
        _stdout_str: str = f"\nОтвет: {stdout}" if stdout is not None else ""
        _stderr_str: str = f"\nОшибки: {stderr}" if stderr is not None else ""
        logger.info(f"Команда: {command}{_stdout_str}{_stderr_str}")


class AttributeType(Enum):
    OPTION = "option"
    SETTING = "setting"

    def __str__(self):
        return f"{self.__class__.__name__}({self._value_})"

    def __repr__(self):
        return f"<{self.__class__.__name__}[{self._name_}]"


class AttributeCli(NamedTuple):
    attribute_type: AttributeType
    name: str
    value: str | int | bool | None = None

    def __str__(self):
        if self.attribute_type == AttributeType.OPTION:
            if isinstance(self.value, bool):
                if not self.value:
                    return attributify(f"{self.name}!")
                else:
                    return attributify(self.name)
            elif isinstance(self.value, str):
                if " " in self.value:
                    return attributify(f"{self.name}=\"{self.value}\"")
                else:
                    return attributify(f"{self.name}={self.value}")
            elif isinstance(self.value, int):
                return attributify(f"{self.name}={self.value}")
            else:
                return attributify(self.name)
        elif self.attribute_type == AttributeType.SETTING:
            return f"--{self.name.removeprefix('--')}={self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"


class TerminalCommand:
    def __init__(
            self,
            command: str,
            flags: Iterable[str] = None,
            attributes: Iterable[AttributeCli] = None,
            key_values: Mapping[str, str | int | bool | None] = None):
        if flags is None:
            flags: set[str] = set()
        if attributes is None:
            attributes: set[AttributeCli] = set()
        if key_values is None:
            key_values: dict[str, str | int | bool | None] = dict()

        self._command: str = command
        self._flags: set[str] = {*flags}
        self._attributes: set[AttributeCli] = {*attributes}
        self._key_values: dict[str, str | int | bool | None] = {**key_values}

    def __add__(self, other):
        if isinstance(other, str):
            self._flags.add(other)
        elif isinstance(other, AttributeCli):
            self._attributes.add(other)
        elif isinstance(other, Mapping):
            self._key_values.update(**other)
        elif isinstance(other, Iterable):
            self._flags.update(filter(lambda x: isinstance(x, str), other))
            self._attributes.update(filter(lambda x: isinstance(x, AttributeCli), other))
            self._key_values.update(filter(lambda x: isinstance(x, Mapping), other))
        else:
            logger.error(f"Невозможно добавить значение {other} типа {type(other)}")
            raise InvalidAddedCommandItemError

    def __str__(self):
        to_attributes: list[AttributeCli] = [
            AttributeCli(AttributeType.OPTION, name, value)
            for name, value in self._key_values.items()]
        self + to_attributes
        _flags: str = stringify_iterable(map(lambda x: f'-{x.removeprefix("-")}', self._flags))
        _attributes: str = stringify_iterable(map(str, self._attributes))

        return stringify_iterable((self._command, _flags, _attributes))

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._command})>"

    def __hash__(self):
        return hash(self._command)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return str(self) == str(other)
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return str(self) != str(other)
        else:
            return NotImplemented

    def validate(self):
        run_command(f"which {self._command}")

    def execute_command(self):
        run_command(str(self))


if __name__ == '__main__':
    asciidoctor: TerminalCommand = TerminalCommand("asciidoctor")
    asciidoctor.validate()
