# -*- coding: utf-8 -*-
from glob import iglob
from pathlib import Path
from typing import Iterator

from doc_builder.constants import validate_path, PathType


class ArchivePathManager:
    _file_patterns: tuple[str, ...] = (
        "**",
        ".adoc",
        "*.*"
    )

    def __init__(self, path: str | Path):
        self._path: Path = validate_path(path, PathType.DIRECTORY)

    def __str__(self):
        return f"{self._path}"

    __repr__ = __str__

    def join_path(self, path: str):
        return self._path.joinpath(path)

    def all_files(self):
        return [
            self.join_path(file)
            for file in iglob("**/*.*", root_dir=self._path, recursive=True)]

    def get_files(self, pattern: str) -> list[Path]:
        return [
            self.join_path(file)
            for file in iglob(pattern, root_dir=self._path, recursive=True)]

    def iter_files(self, pattern: str) -> Iterator[Path]:
        return iter(
            self.join_path(file)
            for file in iglob(pattern, root_dir=self._path, recursive=True))
