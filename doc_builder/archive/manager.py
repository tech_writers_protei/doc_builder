# -*- coding: utf-8 -*-
from pathlib import Path

from loguru import logger

from doc_builder.constants import validate_path, PathType, ArchiveFormat
from doc_builder.errors import MissingArchiveFileError


class ArchiveManager:
    def __init__(self):
        self._archive: str | Path | None = None

    @property
    def archive(self):
        return self._archive

    @archive.setter
    def archive(self, value):
        if isinstance(value, (str, Path)):
            self._archive = validate_path(value, PathType.FILE)
        else:
            raise ValueError

    @property
    def archive_format(self) -> ArchiveFormat:
        return ArchiveFormat(self._suffixes)

    @property
    def _suffixes(self) -> str:
        return "".join(self._archive.suffixes)

    @property
    def unpacked_path(self) -> Path:
        if not bool(self):
            logger.error(f"Файл архива не задан")
            raise MissingArchiveFileError
        else:
            return self._archive.parent.joinpath(self._archive.name.removesuffix(self._suffixes))

    def unpack(self):
        if not bool(self):
            logger.error(f"Файл архива не задан")
            raise MissingArchiveFileError

        if self.archive_format == ArchiveFormat.ZIP:
            from zipfile import ZipFile

            with ZipFile(self._archive, "r") as zf:
                zf.extractall(path=self.unpacked_path)
        else:
            from tarfile import TarFile

            with TarFile(self._archive, "r") as tf:
                tf.extractall(path=self.unpacked_path)

    def __bool__(self):
        return self._archive is not None

    def __str__(self):
        return f"{self._archive}"

    __repr__ = __str__

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return True

    def nullify(self):
        self._archive = None


archive_manager: ArchiveManager = ArchiveManager()
