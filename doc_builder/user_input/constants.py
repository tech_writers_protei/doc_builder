# -*- coding: utf-8 -*-
from doc_builder.constants import DocumentType, DocumentFormat

dd_document_type: tuple[str, ...] = (
    DocumentType.ADMIN_GUIDE.value,
    DocumentType.TECH_DESCRIPTION.value,
    DocumentType.USER_GUIDE.value
)

dd_document_format: tuple[str, ...] = (
    DocumentFormat.DOCX.value,
    DocumentFormat.HTML.value,
    DocumentFormat.PDF.value
)
