# -*- coding: utf-8 -*-
from tkinter import Misc, StringVar, Tk, Widget, OptionMenu
from typing import Iterable


def activate(widgets: Iterable[Widget]):
    for widget in widgets:
        # noinspection PyArgumentList
        widget.configure(state="active")


def deactivate(widgets: Iterable[Widget]):
    for widget in widgets:
        # noinspection PyArgumentList
        widget.configure(state="disabled")


class DropDown(OptionMenu):
    def __init__(self, master: Misc, *values: str):
        variable: StringVar = StringVar()
        direction: str = "below"
        super().__init__(master, variable, *values, direction)


class AppGUI:
    def __init__(self):
        self._root = Tk()
        self._widgets: dict[str, Widget] = dict()

    def set_drop_down(self, drop_down: DropDown):
        self._widgets[drop_down.widgetName] = drop_down

    def grid(self):
        for index, widget_name in enumerate(self._widgets.keys()):
            widget: Widget = self._widgets.get(widget_name)
            widget.grid(column=0, row=index)
        self.grid()
