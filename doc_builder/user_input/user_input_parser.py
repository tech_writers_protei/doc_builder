# -*- coding: utf-8 -*-
from typing import NamedTuple

from doc_builder.constants import DocumentType, DocumentFormat


class UserInputParser(NamedTuple):
    document_type: str
    document_format: str
    product: str

    @property
    def type(self) -> DocumentType:
        return DocumentType(self.document_type)

    @property
    def format(self) -> DocumentFormat:
        return DocumentFormat(self.document_format)

    def __str__(self):
        return f"{self.document_type}, {self.document_format}, {self.product}"

    __repr__ = __str__
