# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection for whole file
from json import JSONDecodeError, load as jload, dumps as jdumps
from pathlib import Path
from typing import Any, Sequence, Iterable, Mapping

from loguru import logger
from tomlkit.exceptions import TOMLKitError
from tomlkit.api import load as tload, dump as tdump

from doc_builder.constants import iter_getitem, validate_path, PathType
from doc_builder.errors import InvalidConfigFileKeyError
from doc_builder.template.item import TemplateFileFiles, TemplateChapter, TemplateItem


class TemplateFile:
    def __init__(self, path: str | Path):
        self._path: Path = validate_path(path, PathType.FILE)
        self._content: dict[str, Any] = dict()

    def __str__(self):
        return f"Файл {self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    @property
    def origin_type(self) -> str:
        return self._path.suffix.removeprefix(".")

    @property
    def name(self) -> str:
        return self._path.name

    def read(self) -> None:
        raise NotImplementedError

    def write(self) -> None:
        raise NotImplementedError

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._content.get(item)
        elif isinstance(item, Sequence):
            if all(isinstance(el, str) for el in item):
                return iter_getitem(self._content, item)
            else:
                _types: str = " ".join([f"{type(i)}" for i in item])
                logger.error(f"Ключи {item} должны быть str, но получено {_types}")
                raise InvalidConfigFileKeyError
        else:
            logger.error(f"Ключ {item} не найден")
            raise InvalidConfigFileKeyError

    get = __getitem__

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        if isinstance(value, str):
            self._content = [value]
        elif isinstance(value, Iterable):
            self._content = [*value]
        else:
            raise ValueError

    @property
    def path(self):
        return self._path

    def __add__(self, other):
        if isinstance(other, Mapping):
            self._content.update(other)
        else:
            logger.error(f"Невозможно добавить элемент {other} типа {type(other)}")

    def template_item(self):
        name: str = self["name"]
        doctype: list[str] = self["doctype"]
        docbranch: list[str] = self["docbranch"]
        docformat: list[str] = self["docformat"]
        version: str = self["version"]
        pagenums: bool = self["pagenums"]
        sectnums: bool = self["sectnums"]
        header: bool = self["header"]
        footer: bool = self["footer"]
        language: str = self["language"]
        structures: list[dict[str, Any]] = self["structure"]
        structure: list[TemplateChapter] = []

        for el in structures:
            chapter: int = el.get("chapter")
            title: str = el.get("title")
            include: str = el.get("include")
            exclude: str = el.get("exclude")
            files: list[dict[str, str]] = el.get("files")
            template_files: list[TemplateFileFiles] = []

            for file in files:
                folder: str = file.get("folder")
                include_files: str = file.get("include_files")
                exclude_files: str = file.get("exclude_files")
                template_files.append(TemplateFileFiles(folder, include_files, exclude_files))

            template_chapter: TemplateChapter = TemplateChapter(chapter, title, template_files, include, exclude)
            structure.append(template_chapter)

        kwargs: dict[str, Any] = {
            "name": name,
            "doctype": doctype,
            "docbranch": docbranch,
            "docformat": docformat,
            "version": version,
            "pagenums": pagenums,
            "sectnums": sectnums,
            "header": header,
            "footer": footer,
            "language": language,
            "structure": structure
        }

        return TemplateItem(**kwargs)


class JSONFile(TemplateFile):
    def read(self) -> None:
        try:
            with open(self._path, "r") as f:
                self._content = jload(f)
            logger.info(f"Прочитан файл {self._path}")
        except JSONDecodeError as e:
            logger.error(f"Ошибка декодирования: {e.msg}")
            raise
        except FileNotFoundError:
            logger.error(f"Файл {self._path} не найден")
            raise
        except PermissionError:
            logger.error(f"Доступ к файлу {self._path} запрещен")
            raise
        except RuntimeError:
            logger.error(f"Время обработки файла {self._path} истекло")
            raise
        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}({e.errno}): {e.strerror}")
            raise

    def write(self) -> None:
        try:
            with open(self._path, "w") as f:
                f.write(jdumps(self._content, ensure_ascii=False, indent=2))
            logger.info(f"Записан файл {self._path}")
        except FileNotFoundError:
            logger.error(f"Файл {self._path} не найден")
            raise
        except PermissionError:
            logger.error(f"Доступ к файлу {self._path} запрещен")
            raise
        except RuntimeError:
            logger.error(f"Время обработки файла {self._path} истекло")
            raise
        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}, {e.strerror}")
            raise


class TOMLFile(TemplateFile):
    def read(self) -> None:
        try:
            with open(self._path, "r") as f:
                self._content = tload(f).items()
            logger.info(f"Прочитан файл {self._path}")
        except TOMLKitError as e:
            logger.error(f"Ошибка чтения: {e.args}")
            raise
        except FileNotFoundError:
            logger.error(f"Файл {self._path} не найден")
            raise
        except PermissionError:
            logger.error(f"Доступ к файлу {self._path} запрещен")
            raise
        except RuntimeError:
            logger.error(f"Время обработки файла {self._path} истекло")
            raise
        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}({e.errno}): {e.strerror}")
            raise

    def write(self) -> None:
        try:
            with open(self._path, "w") as f:
                tdump(self._content, f)
            logger.info(f"Записан файл {self._path}")
        except TOMLKitError as e:
            logger.error(f"Ошибка записи: {e.args}")
            raise
        except FileNotFoundError:
            logger.error(f"Файл {self._path} не найден")
            raise
        except PermissionError:
            logger.error(f"Доступ к файлу {self._path} запрещен")
            raise
        except RuntimeError:
            logger.error(f"Время обработки файла {self._path} истекло")
            raise
        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}, {e.strerror}")
            raise
