# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Any

from jsonschema.exceptions import ValidationError, SchemaError
from jsonschema.validators import validate
from loguru import logger

from doc_builder.errors import TemplateSchemeFileError
from doc_builder.template.converter import ConfigFileConverter, config_file_converter, TOMLtoJSONConverter, \
    toml_to_json_converter
from doc_builder.template.file import TemplateFile, JSONFile


class TemplateValidator:
    def __init__(
            self,
            schema_file: str | Path,
            from_: str,
            to: str,
            converter: ConfigFileConverter = config_file_converter):
        _path: Path = Path(schema_file)

        if not _path.exists():
            logger.error(f"Файл {_path} не найден")
            raise TemplateSchemeFileError

        if _path.suffix != ".json":
            logger.error(f"Файл {_path} должен иметь расширение .json, но получено {_path.suffix}")
            raise TemplateSchemeFileError

        self._json_scheme_path: Path = Path(_path)
        self._template_file: str | Path | None = None
        self._converter: ConfigFileConverter = converter
        self._converter.set_from(from_)
        self._converter.set_to(to)

    def __str__(self):
        return f"{self.__class__.__name__}: {self._json_scheme_path}"

    __repr__ = __str__

    @property
    def template_file(self):
        return self._template_file

    @template_file.setter
    def template_file(self, value):
        self._template_file = value

    @property
    def converted_file(self) -> TemplateFile:
        self._converter.convert()
        return self._converter.converted_file

    @property
    def instance(self) -> dict[str, Any]:
        self.converted_file.read()
        return self.converted_file.content

    @property
    def schema(self) -> dict[str, Any]:
        json_file: JSONFile = JSONFile(self._json_scheme_path)
        json_file.read()
        return json_file.content

    @property
    def converter(self):
        return self._converter

    @converter.setter
    def converter(self, value):
        self._converter = value

    def nullify(self):
        self._template_file: TemplateFile | None = None
        self._converter: ConfigFileConverter = config_file_converter
        self._converter.nullify()

    def validate_file(self) -> bool:
        __flag: bool = False

        try:
            validate(self.instance, self.schema)
        except ValidationError as e:
            logger.error(f"Ошибка валидации: {e.message}")
        except SchemaError as e:
            logger.error(f"Ошибка схемы валидации: {e.message}")
        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}({e.errno}): {e.strerror}")
        else:
            __flag: bool = True
        finally:
            return __flag


class TOMLValidator(TemplateValidator):
    def __init__(self):
        schema_file: Path = Path(__file__).parent.joinpath("source").joinpath("template.json")
        from_: str = "toml"
        to: str = "json"
        converter: TOMLtoJSONConverter = toml_to_json_converter
        super().__init__(schema_file, from_, to, converter)
