# -*- coding: utf-8 -*-
from glob import iglob
from pathlib import Path
from typing import Any, NamedTuple

from loguru import logger

from doc_builder.constants import DocumentType, DocumentFormat, DocumentLanguage, DocumentBranch
from doc_builder.errors import InvalidAddedChapterError, InvalidRegexStringError, \
    MissingRequiredTemplateAttributeError


class RegexString(str):
    regex_string: str = (
        "0123456789"
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "!\"#'()*+,-./:<=>?[]^_|")

    def __new__(cls, __string: str):
        if any(s not in cls.regex_string for s in __string):
            logger.error(f"Некорректная строка {__string}")
            raise InvalidRegexStringError

        instance = super().__new__(cls, __string)
        return instance


class TemplateFileFiles(NamedTuple):
    folder: str | Path
    include_files: RegexString | str | None = "**/*.adoc"
    exclude_files: RegexString | str | None = None

    def __str__(self):
        return (
            f"Folder: {self.folder}\n"
            f"Include: {self.include_files}\n"
            f"Exclude: {self.exclude_files}")

    __repr__ = __str__

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.folder == other.folder
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.folder != other.folder
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.folder)

    def file_paths(self):
        include_files: set[str] | None = set(
            iglob(self.include_files, root_dir=self.folder, recursive=True))

        if self.exclude_files is None:
            exclude_files: set[str] = set()
        else:
            exclude_files: set[str] | None = set(
                iglob(self.exclude_files, root_dir=self.folder, recursive=True))

        return include_files.difference(exclude_files)


class TemplateChapter(NamedTuple):
    chapter: int
    title: str
    files: list[TemplateFileFiles] = []
    include: RegexString | str | None = "**/*.adoc"
    exclude: RegexString | str | None = None

    def __str__(self):
        return (
            f"Chapter: {self.chapter}\n"
            f"Title: \n"
            f"{str(self.file_paths)}\n"
            f"Include: {self.include}\n"
            f"Exclude: {self.exclude}")

    __repr__ = __str__

    def __hash__(self):
        return hash((self.title, self.chapter))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.chapter == other.chapter
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.chapter != other.chapter
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.chapter < other.chapter
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.chapter <= other.chapter
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.chapter > other.chapter
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.chapter >= other.chapter
        else:
            return NotImplemented

    def file_paths(self):
        _files: set[str] = set()

        for file in self.files:
            include_files: set[str] | None = set(
                iglob(self.include, root_dir=file.folder, recursive=True))

            if self.exclude is None:
                exclude_files: set[str] = set()
            else:
                exclude_files: set[str] | None = set(
                    iglob(self.exclude, root_dir=file.folder, recursive=True))

            _files.union(*file.file_paths().union(include_files.difference(exclude_files)))

        return _files


class TemplateItem:
    __slots__: tuple[str, ...] = (
        "_name",
        "_doctype",
        "_docbranch",
        "_docformat",
        "_version",
        "_pagenums",
        "_sectnums",
        "_header",
        "_footer",
        "_language",
        "_structure")

    _required: tuple[str, ...] = (
        "name",
        "doctype",
        "docbranch",
        "docformat",
        "version")

    _optional_bool: tuple[str, ...] = (
        "pagenums",
        "sectnums",
        "header",
        "footer"
    )

    def __init__(self, **kwargs):
        _missing: list[str] = [item for item in self._required if item not in kwargs]

        if _missing:
            _missing_str: str = ", ".join(_missing)
            logger.error(f"Отсутствуют обязательные параметры:\n{_missing_str}")
            raise MissingRequiredTemplateAttributeError

        del _missing

        self._name: str = kwargs.get("name")
        self._doctype: list[DocumentType] = [
            DocumentType[item] for item in kwargs.get("doctype")]
        self._docbranch: list[DocumentBranch] = [
            DocumentBranch[item] for item in kwargs.get("docbranch")]
        self._docformat: list[DocumentFormat] = [
            DocumentFormat[item] for item in kwargs.get("docformat")]
        self._version: str = kwargs.get("version")

        for item in self._optional_bool:
            if item in kwargs:
                setattr(self, f"_{item}", bool(kwargs.get(item)))
            else:
                setattr(self, f"_{item}", True)

        if "language" in kwargs:
            self._language: DocumentLanguage = DocumentLanguage[kwargs.get("language")]
        else:
            self._language: DocumentLanguage = DocumentLanguage.RU

        if "structure" in kwargs:
            self._structure: list[TemplateChapter] = [*kwargs.get("structure")]
        else:
            self._structure: list[TemplateChapter] = []

    def __str__(self):
        _asdict: dict[str, Any] = {
            f"{attr.removesuffix('_')}": getattr(self, attr) for attr in self.__slots__}
        _asdict_str: str = "\n".join([f"{k} = {v}" for k, v in _asdict.items()])
        return f"{self.__class__.__name__}:\n{_asdict_str}"

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._name == other._name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._name != other._name
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, TemplateChapter):
            self._structure.append(other)
        else:
            logger.error(f"Элемент {other} типа {type(other)} нельзя добавить")
            raise InvalidAddedChapterError

    @property
    def name(self):
        return self._name

    @property
    def doctype(self):
        return self._doctype

    @property
    def docformat(self):
        return self._docformat
