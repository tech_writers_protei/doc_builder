# -*- coding: utf-8 -*-
from pathlib import Path

from loguru import logger

from doc_builder.errors import InvalidConfigFileExtensionError
from doc_builder.template.file import TemplateFile, TOMLFile, JSONFile


class ConfigFileConverter:
    def __init__(self):
        self._config_file: TemplateFile | None = None
        self._path: str | Path | None = None
        self._from: str | None = None
        self._to: str | None = None
        self._converted_file: TemplateFile | None = None

    def nullify(self):
        self._config_file: TemplateFile | None = None
        self._path: str | Path | None = None
        self._from: str | None = None
        self._to: str | None = None
        self._converted_file: TemplateFile | None = None

    def set_path(self, path: str | Path):
        _suffixes: dict[str, type] = {
            ".toml": TOMLFile,
            ".json": JSONFile
        }

        _cls: type = _suffixes.get(path.suffix, None)
        if _cls is None:
            logger.error(f"Некорректный тип файла {path.suffix}")
            raise InvalidConfigFileExtensionError

        self._config_file: TemplateFile = _cls(path)
        self._config_file.read()
        self._from: str = self._config_file.origin_type

    def set_config_file(self, config_file: TemplateFile):
        self._config_file = config_file

    def set_from(self, from_: str):
        self._from: str = from_

    def set_to(self, to: str):
        self._to: str = to

    def convert(self):
        _files: dict[str, type] = {
            "json": JSONFile,
            "toml": TOMLFile
        }

        cls: type = _files.get(self._to)

        self._converted_file: TemplateFile = cls(self._path)
        self._converted_file._content = self._config_file.content
        self._converted_file.write()

    @property
    def converted_file(self):
        return self._converted_file

    def __str__(self):
        return f"{self._config_file}:\n{self._from} -> {self._to}"

    def __repr__(self):
        return f"<{self._config_file}({self._from}, {self._to})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._from, self._to == other._from, other._to
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._from, self._to != other._from, other._to
        else:
            return NotImplemented


class TOMLtoJSONConverter(ConfigFileConverter):
    def set_to(self, **kwargs):
        self._to: str = "json"

    def set_from(self, **kwargs):
        self._from: str = "toml"


config_file_converter: ConfigFileConverter = ConfigFileConverter()
toml_to_json_converter: TOMLtoJSONConverter = TOMLtoJSONConverter()
