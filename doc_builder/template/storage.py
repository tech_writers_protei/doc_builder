# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable

from loguru import logger

from doc_builder.constants import validate_path, PathType, DocumentType, DocumentFormat
from doc_builder.template.file import TemplateFile
from doc_builder.template.item import TemplateItem


class TemplateStorage(dict):
    def __init__(self, path_dir: str | Path = None, template_files: Iterable[TemplateFile] = None):
        if template_files is None:
            template_files: list[TemplateFile] = []

        if path_dir is None:
            path_dir: Path = Path(__file__).parent.joinpath("source")

        kwargs: dict[str, TemplateFile] = {
            template_item.name: template_item for template_item in template_files}

        super().__init__(**kwargs)
        path_dir: Path = validate_path(path_dir, PathType.DIRECTORY)
        self._path_dir: Path = path_dir

    def __str__(self):
        return f"{self.__class__.__name__}"

    __repr__ = __str__

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.keys()

    def __getitem__(self, item):
        if isinstance(item, str):
            return self.get(item)
        elif isinstance(item, DocumentType):
            return [k for k, v in self.items() if item in v.doctype]
        elif isinstance(item, DocumentFormat):
            return [k for k, v in self.items() if item in v.docformat]
        else:
            logger.error(f"Ключ {item} типа {type(item)} не найден")
            raise KeyError

    def __add__(self, other):
        if isinstance(other, TemplateItem):
            if other.name in self.keys():
                logger.info(f"Шаблон {other.name} уже присутствует")
            else:
                self[other.name] = other
        else:
            return NotImplemented
