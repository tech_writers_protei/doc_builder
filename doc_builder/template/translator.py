# -*- coding: utf-8 -*-
from enum import Enum
from typing import Any

from doc_builder.template.file import TemplateFile
from doc_builder.template.item import TemplateItem, TemplateChapter, TemplateFileFiles


class TranslateDirection(Enum):
    FROM_ITEM_TO_FILE = "item-file"
    FROM_FILE_TO_ITEM = "file-item"
    NONE = "NONE"
    DONE = "DONE"


class TemplateTranslator:
    __slots__ = ["_template_file", "_template_item"]

    def __init__(self):
        self._template_file: TemplateFile | None = None
        self._template_item: TemplateItem | None = None

    @property
    def template_file(self):
        return self._template_file

    @template_file.setter
    def template_file(self, value):
        self._template_file = value

    @property
    def template_item(self):
        return self._template_item

    @template_item.setter
    def template_item(self, value):
        self._template_item = value

    def __bool__(self):
        return self._template_file, self._template_item is not None, None

    def _check_attribute(self, attribute: str):
        if attribute not in self.__slots__:
            raise AttributeError
        else:
            return getattr(self, attribute) is not None

    def direction(self):
        if not bool(self):
            return TranslateDirection.NONE
        elif self._check_attribute("_template_file"):
            return TranslateDirection.FROM_FILE_TO_ITEM
        elif self._check_attribute("_template_item"):
            return TranslateDirection.FROM_ITEM_TO_FILE
        else:
            return TranslateDirection.DONE

    def convert_file_to_item(self):
        name: str = self._template_file.get("name")
        doctype: list[str] = self._template_file.get("doctype")
        docbranch: list[str] = self._template_file.get("docbranch")
        docformat: list[str] = self._template_file.get("docformat")
        version: str = self._template_file.get("version")
        pagenums: bool = self._template_file.get("pagenums")
        sectnums: bool = self._template_file.get("sectnums")
        header: bool = self._template_file.get("header")
        footer: bool = self._template_file.get("footer")
        language: str = self._template_file.get("language")
        structures: list[dict[str, Any]] = self._template_file.get("structure")
        structure: list[TemplateChapter] = []

        for el in structures:
            chapter: int = el.get("chapter")
            title: str = el.get("title")
            include: str = el.get("include")
            exclude: str = el.get("exclude")
            files: list[dict[str, str]] = el.get("files")
            template_files: list[TemplateFileFiles] = []

            for file in files:
                folder: str = file.get("folder")
                include_files: str = file.get("include_files")
                exclude_files: str = file.get("exclude_files")
                template_files.append(TemplateFileFiles(folder, include_files, exclude_files))

            template_chapter: TemplateChapter = TemplateChapter(chapter, title, template_files, include, exclude)
            structure.append(template_chapter)

        kwargs: dict[str, Any] = {
            "name": name,
            "doctype": doctype,
            "docbranch": docbranch,
            "docformat": docformat,
            "version": version,
            "pagenums": pagenums,
            "sectnums": sectnums,
            "header": header,
            "footer": footer,
            "language": language,
            "structure": structure
        }

        return TemplateItem(**kwargs)
