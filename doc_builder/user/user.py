# -*- coding: utf-8 -*-
from enum import Enum
from typing import NamedTuple

from loguru import logger


class UserAction(Enum):
    READ = "read"
    CREATE = "create"
    UPDATE = "update"
    DELETE = "delete"
    UPDATE_ANY = "update_any"
    DELETE_ANY = "delete_any"
    CHANGE_PRIVILEGES = "change_privileges"


class UserLevel(NamedTuple):
    name: str
    actions: set[UserAction] = {}
    logins: set[str] = {}

    def __str__(self):
        _actions_str: str = ", ".join(f"{action}" for action in self.actions)
        _logins_str: str = ", ".join(f"{login}" for login in self.logins)
        return f"{self.name}:\nactions = {_actions_str}\nlogins = {_logins_str}"

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.logins
        elif isinstance(item, UserAction):
            return item in self.actions
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, str):
            self.logins.add(other)
            logger.info(f"Добавлен пользователь {other}")
        elif isinstance(other, UserAction):
            self.actions.add(other)
            logger.info(f"Добавлено действие {str(other)}")
        else:
            logger.error(f"Невозможно добавить {other} типа {type(other)}")


