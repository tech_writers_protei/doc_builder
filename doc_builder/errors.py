# -*- coding: utf-8 -*-
class BaseError(Exception):
    """Base error class to inherit."""


class BaseFrontMatterError(BaseError):
    """Base frontmatter error class to inherit."""


class RequiredAttributeMissingError(BaseError):
    """Required attribute has no specified value."""


class PrivateTokenMissingError(BaseError):
    """Private token is required but not found."""


class InvalidEnumValueError(BaseError):
    """String value is not defined in the associated Enum class."""


class TemplateSchemeFileError(BaseError):
    """XSD scheme file is not found or invalid."""


class InvalidAttributeError(BaseError):
    """Attribute is not proper."""


class MissingFrontMatterError(BaseFrontMatterError):
    """File has no front matter."""


class InvalidFileStructureError(BaseError):
    """YAML borders are placed in wrong lines."""


class InvalidFrontMatterLineError(BaseFrontMatterError):
    """Line of the front matter does not have a colon."""


class InvalidFrontMatterEntityError(BaseFrontMatterError):
    """Front matter does not have a title key."""


class InvalidFrontMatterError(BaseFrontMatterError):
    """Front matter cannot be None."""


class MissingRequiredComponentsError(BaseError):
    """Path does not have required components."""


class InvalidIncludeFileError(BaseError):
    """IncludeFileItem item is not found."""


class InvalidAddedCommandItemError(BaseError):
    """Command item cannot be added."""


class InvalidAddedChapterError(BaseError):
    """Structure item cannot be added."""


class MissingAsciiDocEnumValuesError(BaseError):
    """AsciiDoc attribute has no required enum values."""


class InvalidRegexStringError(BaseError):
    """Regex string has invalid symbols."""


class InvalidConfigFileExtensionError(BaseError):
    """File extension must be json or toml."""


class InvalidConfigFileKeyError(BaseError):
    """Config file key is not valid."""


class MissingRequiredTemplateAttributeError(BaseError):
    """Template file has missing required attributes."""


class InvalidAsciiDocAttributeLineError(BaseError):
    """AsciiDoc attribute line is invalid."""


class MissingArchiveFileError(BaseError):
    """Archive file is not specified."""


class BasePathError(BaseError):
    """Base path error class to inherit."""


class MissingPathTypeError(BasePathError):
    """Path type is neither file nor directory."""


class MissingPathError(BasePathError):
    """Path does not lead to the real item."""


class InvalidPathTypeError(BasePathError):
    """Path type does not match to the path item."""


class InvalidSkipIndexError(BaseError):
    """Skip index precedes the maximum value."""


class MissingAttributeError(BaseError):
    """Value does not have a required attribute."""


class InvalidHTTPResponseError(BaseError):
    """HTTP response has an improper format or type."""


class NoneValueError(BaseError):
    """Required values are equal to None."""
